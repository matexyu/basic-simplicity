/*
  16x Servos Sg 90: Hello World
  
  Based on:
   . PCA 9685: 16-Channel 12-bit PWM/Servo Driver board, I2C controlled
     . Original Adafruit version: https://www.adafruit.com/product/815
     . PWM/Servo Driver library here: https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library
       Available via Arduino "Install Libraries..." under "Adafruit PWM Servo Driver Library"

  --------------------
    PCA 9685 NOTES:
  --------------------
  Connect the PCA 9685 SDA and SCL inputs to the proper pins, depending on the used Arduino board.
  For example, on the Arduino Nano:
   . SDA: Analog 4 pin
   . SCL: Analog 5 pin

  Important: the PCA 9685 Output Enable (OE) input of the driver *is active LOW*

   Mathieu Bosi 2016
*/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// Called this way, it uses the default address: 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// You can also call it with a different address you want, e.g.
//  Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);

// Depending on your servo make, the pulse width min and max may vary, you 
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!

// Watching the SG90 from the front, side with cabes going down...
const int kMinPulseUs = 600; // Right side (the one with extra room after 180º angle)
const int kMaxPulseUs = 2190; // Left side

const int kRefreshIntervalHz = 50;
const int kRefreshIntervalUs = 1000000L / kRefreshIntervalHz; // 20000
const int kRefreshIntervalMs = kRefreshIntervalUs / 1000L;

const int16_t k12bitMinPwmValue = (4095L * kMinPulseUs) / (long)kRefreshIntervalUs;
const int16_t k12bitMaxPwmValue = (4095L * kMaxPulseUs) / (long)kRefreshIntervalUs;

const int16_t num_servos = 16;
const int16_t pwm_range_width = k12bitMaxPwmValue - k12bitMinPwmValue;
const int16_t inter_servo_pwm_offset = pwm_range_width / 16;

void setup()
{
  pwm.begin();
  pwm.setPWMFreq(kRefreshIntervalHz);  // Analog servos run at ~50 Hz updates
  // must be changed after calling Wire.begin() (inside pwm.begin())
  TWBR = 12; // upgrade to 400KHz!
}

void setPwm(int16_t v)
{
  for (int16_t j = 0; j < num_servos; j++)
  {
    int16_t pwm_value = (v + j * inter_servo_pwm_offset);
    while (pwm_value >= pwm_range_width) pwm_value -= pwm_range_width;
    pwm_value += k12bitMinPwmValue;
    pwm.setPWM(j, 0, pwm_value);
  }
}
  
void loop()
{  
  for (int16_t i = 0; i <= pwm_range_width; i+=4)
  {
    setPwm(i);
    delay(10);
  }
  for (int16_t i = pwm_range_width; i >= 0; i-=4)
  {
    setPwm(i);
    delay(10);
  }

  /*
  for (int16_t j = 0; j < num_servos; j++)
  {
    pwm.setPWM(j, 0, k12bitMinPwmValue);
  }
  delay(1000);
  */
    
}


