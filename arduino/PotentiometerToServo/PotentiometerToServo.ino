
#define SERIAL_DEBUG 0

#define FASTADC 1

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define LED_PIN 13
#define PWM_OUT_PIN 2
#define ANALOG_CONTROL_PIN 0

void setup()
{
#if FASTADC
  // set prescale to 16
  sbi(ADCSRA,ADPS2) ;
  cbi(ADCSRA,ADPS1) ;
  cbi(ADCSRA,ADPS0) ;
#endif

  pinMode(PWM_OUT_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

#if SERIAL_DEBUG
  Serial.begin(115200);
#endif
}

const int kMinDelayMicroseconds = 500;
const int kRefreshIntervalMsecs = 20;

int16_t cycles_to_disable_counter = 0;
int16_t val_old = 0;

void loop() 
{
  int16_t val = analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);
  val += analogRead(ANALOG_CONTROL_PIN);

  val >>= 3;
  val >>= 3;
  val <<= 3;
  
  val *= 2;
  
  val -= 12*2;
  
  if (val < 44) val = 44;
  else if (val > 2000-100) val = 2000-100;

  if (val != val_old)
  {
    val_old = val;
    cycles_to_disable_counter = 25;
    
    digitalWrite(LED_PIN, 1);
  }

  if (cycles_to_disable_counter > 0)
  {
    cycles_to_disable_counter--;
    
    // High period:
    digitalWrite(PWM_OUT_PIN, 1);
    _delay_us(kMinDelayMicroseconds);
    delayMicroseconds(val); // max 2 msecs
    digitalWrite(PWM_OUT_PIN, 0);
  
    // Low period:
    const int16_t usecs_missing_to_2500_usecs = 2000 - val;
    if(usecs_missing_to_2500_usecs <= 1000)
    {
      delayMicroseconds(usecs_missing_to_2500_usecs);
    }
    else
    {
      _delay_us(1000);
      delayMicroseconds(usecs_missing_to_2500_usecs - 1000);  
    }
    
    delay(kRefreshIntervalMsecs);
    
#if SERIAL_DEBUG
    Serial.println(val, DEC);
#endif
  }
  else
  {
    digitalWrite(LED_PIN, 0);
  }
}

