/**
 * 16x Sg90 Servos Screen control, Hello World. 
 * 
 */

import processing.serial.*;

// Watching the SG90 from the front, side with cables going down...
final int kMinPulseUs = 550; // Right side (the one with extra room after 180º angle)
final int kMaxPulseUs = 2170; // Left side

final int kRefreshIntervalHz = 50; // Analog servos run at ~50 Hz updates
final int kRefreshIntervalUs = 1000000 / kRefreshIntervalHz; // 20000
final int kRefreshIntervalMs = kRefreshIntervalUs / 1000;

final int k12bitMinPwmValue = (4095 * kMinPulseUs) / kRefreshIntervalUs;
final int k12bitMaxPwmValue = (4095 * kMaxPulseUs) / kRefreshIntervalUs;

final float kScreenMargin = 2.0; // the larger, the "farther" away in the computer screen

Serial myPort;  // Create object from Serial class

void setup() 
{
  size(1024, 1024, P3D);
  frameRate(60);
  
  // Print a list of the serial ports, for debugging purposes:
  printArray(Serial.list());
  
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 115200);
  myPort.clear();
  
  for (int i = 0; i < 16; i++)
  {
    bufferPwm[i] = k12bitMaxPwmValue;
  }
}

float buffer[] = new float[4 * 4];
int bufferPwm[] = new int[4 * 4];
byte frameBytes[] = new byte[3 * 16];

float pwmToAngle(int pwm_value)
{
  return map(pwm_value, 
    k12bitMinPwmValue,
    k12bitMaxPwmValue,
    PI*0.5, -PI*0.5) + PI*0.5;
}

// http://yourdailygeekery.com/2011/06/28/modulo-of-negative-numbers.html
float modulo(float a, float b)
{
  float c = a % b;
  return (c < 0) ? c + b : c;
}

int clamp(int v, int a, int b)
{
  if(a > b) return clamp(v, b, a);
  return v < a ? a : v > b? b : v;
}

float clamp(float v, float a, float b)
{
  if(a > b) return clamp(v, b, a);
  return v < a ? a : v > b? b : v;
}

int angleToPwm(float angle)
{
  float a = modulo(angle, PI);
  
  float v = map(a - PI*0.5,
    PI*0.5, -PI*0.5,
    k12bitMinPwmValue,
    k12bitMaxPwmValue);
    
    int vi = (int)(v + 0.5);
    vi = clamp(vi, k12bitMinPwmValue, k12bitMaxPwmValue);
    
    return vi;
}

void drawServo(float x, float y, float angle, float shaft_length)
{
  pushMatrix();
  translate(x, y);
  rotateZ(angle);
  line(-shaft_length*0.5, 0, shaft_length*0.5, 0);
  popMatrix();
}

void drawSimulation()
{
  noFill();
  strokeWeight(5);
  stroke(200);
  
  final float h = height;
  final float w = h;
  
  float sw = w / (6.0 + kScreenMargin * 2.0);
  for (int y = 0; y < 4; y++)
  {
    float sy = map(y, -1.0 - kScreenMargin, 4.0 + kScreenMargin, 0, h);
    for (int x = 0; x < 4; x++)
    {
      float sx = width * 0.5 + map(x, -1.0 - kScreenMargin, 4.0 + kScreenMargin, -w * 0.5, w * 0.5);
      drawServo(sx, sy, pwmToAngle(bufferPwm[y*4 + x]), sw);
    }
  }
}

void generateAnimation()
{
  final float t = millis() / 1000.0;
  
  for (int i = 0; i < 16; i++)
  {
    float k = sin(t * PI * 2.0 + sin(t*.125*PI) * (PI * i / 16.0)) * 0.5 + 0.5;
    buffer[i] = k; 
  }
  
  final float range = k12bitMaxPwmValue - k12bitMinPwmValue;
  for (int i = 0; i < 16; i++)
  {
    int pwm_value = (int)(buffer[i] * range + k12bitMinPwmValue); 
    bufferPwm[i] = pwm_value;
  }
}

void animateFromMouse()
{
  final float h = height;
  final float w = h;

  for (int y = 0; y < 4; y++)
  {
    float sy = map(y, -1.0 - kScreenMargin, 4.0 + kScreenMargin, 0, h);
    for (int x = 0; x < 4; x++)
    {
      float sx = width * 0.5 + map(x, 
                                   -1.0 - kScreenMargin, 4.0 + kScreenMargin, 
                                   -w * 0.5, w * 0.5);
      
      float dx = mouseX - sx;
      float dy = mouseY - sy;
      
      float angle = atan2(dy, dx);
      
      bufferPwm[y*4 + x] = angleToPwm(angle);
    }
  }
}

void sendToServoScreen()
{
  for (int i = 0; i < 16; i++)
  {    
    int j = i * 3;
    int pwm_value = bufferPwm[i];
    
    frameBytes[j] = (byte)((i & 0x7f) | 128);
    j++;
    frameBytes[j] = (byte)((pwm_value >> 7) & 0x7f);
    j++;
    frameBytes[j] = (byte)(pwm_value & 0x7f); 
  }

  myPort.write(frameBytes);
}

void draw() 
{
  background(0);
  
  //generateAnimation();
  
  animateFromMouse();
  
  sendToServoScreen();
  
  drawSimulation();
}